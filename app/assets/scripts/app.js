import "../styles/styles.css";
import "lazysizes";
import $ from 'jquery';

$(window).on('load', function() {
    $('#main-header').hide();
    $("#preloader").delay(2000).fadeOut('slow'); 
    $('#main-header').delay(2000).fadeIn(0);
});

import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import ActiveLinks from "./modules/ActiveLinks";

let mobileMenu = new MobileMenu();

new RevealOnScroll($("#why"));
new RevealOnScroll($("#service"));
new RevealOnScroll($("#feedback"));
new RevealOnScroll($("#blog"));

new ActiveLinks();

if (module.hot) {
    module.hot.accept();
}
